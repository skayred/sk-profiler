# Bulding #

Type

    mvn clean install assembly:single

in the project directory. Copy file 'target/sk-profiler-1.0-jar-with-dependencies.jar' anywhere you want.

# Usage #

Type run profile with your code just use '-javaagent' directive. Example:

    java -javaagent:/Users/dmitry/projects/sk-profiler/target/sk-profiler-1.0-jar-with-dependencies.jar -jar caeserver-core/target/TaskBuilder-0.2-jar-with-dependencies.jar
